type GridProps = {
    name: string
    page: number
    isLoggedIn: boolean
}

export const Grid = (props: GridProps) => {
    return (
        <div>
            <h2>
                {
                    props.isLoggedIn ? `Belajar ${props.name} TypeScript ${props.page}` : `React`
                }
            </h2>
        </div>
    )
}