import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Grid } from './components/Grid';
import { Status } from './components/Status';
import { Person } from './components/Person';
import { PersonList } from './components/PersonList';
function App() {
  const PersonName = {
    first: 'Faisal',
    last: 'Firaz'
  }

  const nameList = [
    {
      first: 'Batman',
      last : 'Bruce'
    },
    {
      first: 'Superman',
      last : 'Clark'
    },
    {
      first: 'Wonder Woman',
      last : 'Diana'
    }
  ]

  return (
    <div className="App">
      <header className="App-header">
        <Person name={PersonName} />
        <PersonList names={nameList} ></PersonList>
        {/* <Status status='loading'></Status> */}
      {/* <Grid name='React' page={2} isLoggedIn={true}></Grid> */}
      </header>
    </div>
  );
}

export default App;
